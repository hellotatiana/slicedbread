# README #

### What is this repository for? ###

* Test WordPress website development installation
* v.1.0.

### Backend information ###

* If you are new to Vagrant please refer to https://www.vagrantup.com/ for documentation 
* Virtual Machine, Specs and other info: https://box.scotch.io/
* Vagrant is optional and not required to run this installation 

### Database import known issues 

* Theme settings are saved to wp_options table as serialized data with full path URLs. 
* PLEASE UPDATE absolute URLs in Tools - Better Search and Replace 
* Replace 'http://192.168.33.11' with 'yourdomain'