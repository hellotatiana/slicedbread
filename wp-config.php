<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'slicedbread'); // local
//define('DB_NAME', 'u382958_slicedbr'); // stage


/** MySQL database username */
define('DB_USER', 'root'); // local
//define('DB_USER', 'u382958_slicedbr'); // stage

/** MySQL database password */
define('DB_PASSWORD', 'root'); // local
//define('DB_PASSWORD', 'ClaC9wy_rsPeYB'); // stage

/** MySQL hostname */
define('DB_HOST', 'localhost'); // local
//define('DB_HOST', 'u382958.mysql.masterhost.ru); // stage

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$,iDk<wUUG&g$V(d-|M)df;FS;kZT?N!lV<&0*V~Am[ND8im:pG+?dZf(Se8D}KO');
define('SECURE_AUTH_KEY',  'J*-&UYVrckG9{L-;|,EePo$q`=7erT 5/Nd.430{--J+$~WLu4K=9pJ|>BRd8GBf');
define('LOGGED_IN_KEY',    'SgWpQ@kNd4|_o_K{w4>;jx7,N>CjhFuqJBqX@r6C%.WX~B6tsKnOWL*hh2I[iT);');
define('NONCE_KEY',        '=$q|~g//y.$}#+3aFfax|*9)ENg%Z*B#C?]E;jEb5NM%-6WZ6D&0vd|})w6u;fDE');
define('AUTH_SALT',        '%k*zom 7ho?l&zD!=adctf.jD%HD9Z#o<D`xfaP0g]I?fw)]v2P0XL5h`DdLZGlD');
define('SECURE_AUTH_SALT', 'Ov*Wk#K$+?B:>J={]M$-y!b})Ru0o|w:ziRX@dFuMroeEte/K`Bsu?k+GX}5o#BR');
define('LOGGED_IN_SALT',   '~mQiBf0{^_&.lvITXcAo8H`YW3uzS1N2&.WMv[rR8/*9C}wBw^Z{1O=^-kB*9C;q');
define('NONCE_SALT',       'E6C=g:kwk}a G{)j8k4ZkD|~,$5kq ,[0k30Jkl8E%ta;cSD; mU9@EN}Kr;UOH>');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
