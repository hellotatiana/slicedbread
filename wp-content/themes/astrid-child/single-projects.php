<?php get_header(); ?>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>
    <div id="content" <?php post_class(); ?>>
        <section>
            <div class="container atblock">
                <h2><?php the_title(); ?></h2>
                <?php the_content(); // Dynamic Content ?>
                <?php

                // get project meta
                $fields = get_field_objects(); ?>
                <?php if( $fields ): ?>
                    <?php
                    $client = get_field('client');
                    $url    = get_field('project_url'); ?>
                    <div class="project-meta">
                        <?php if ($client) :?>
                            <p><strong>Client:</strong> <span><?php echo $client ?></span></p>
                        <?php endif; ?>
                        <?php if ($url) :?>
                            <p><strong>View project: </strong> <span><a href="<?php echo $url ?>" target="_blank" rel="nofollow">Link</a></span></p>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <?php if (has_post_thumbnail($post->ID)): ?>
                    <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'astrid_large'); ?>
                    <div class="astrid-3col">
                        <p>
                            <img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>">
                        </p>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    </div>
<?php endwhile; ?>

<?php else: ?>
<?php endif; ?>
<?php wp_footer(); ?>
