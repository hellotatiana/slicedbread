<?php

/*

Template Name: Page Projects

*/
get_header();
?>
<div id="primary" class="content-area fullwidth">
    <main id="main" class="site-main" role="main">
        <?php
        $query = new WP_Query(array('post_type' => 'Projects', 'orderby' => 'menu_order', 'order' => 'ASC'));
        if ($query->have_posts()) : ?>
            <div class="container projects">
                <?php $counter = 0; ?>
                <?php while ($query->have_posts()) : $query->the_post(); ?>

                    <?php if ($counter % 3 == 0) :

                        echo $counter > 0 ? "</div>" : ""; // close div if it's not the first
                        echo "<div class='row projects-row'>";
                    endif;
                    ?>
                    <!-- projects loop -->
                    <div class="col-md-4 project-wrapper" id="post-<?php the_ID(); ?>">
                        <?php if (has_post_thumbnail($post->ID)): ?>
                            <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'astrid_large'); ?>
                            <p>
                                <a href="<?php the_permalink(); ?>"><img src="<?php echo $image[0]; ?>"
                                                                         alt="<?php the_title(); ?>"></a>
                            </p>
                        <?php endif; ?>
                        <div class="project-info">
                            <h3 class="project-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <?php if (get_field('client')): ?>
                                <p><strong>Client:</strong> <span><?php the_field('client'); ?></span></p>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php $counter++; ?>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
    </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>

